<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'admin@example.com',
            'email' => 'admin@example.com',
            'password' => bcrypt('admin@example.com'),
            'email_verified_at' => date('Y-m-d h:i:s')
        ]);
    }
}
