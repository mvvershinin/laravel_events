<?php

use App\Models\GeoNode;
use Illuminate\Database\Seeder;

class GeoNodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cities = json_decode(file_get_contents(__DIR__.'/cities.json'));

        foreach($cities as $city){
            $country = GeoNode::firstOrCreate(
                [
                    'name' => $city->country
                ]
            );
            $region = GeoNode::firstOrCreate(
                [
                    'name' => $city->region,
                    'parent_id' => $country->id,
                    'level' => 1
                ]
            );

            GeoNode::firstOrCreate(
                [
                    'name' => $city->name,
                    'parent_id' => $region->id,
                    'level' => 2
                ]
            );
        };
    }

}
