<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(GeoNodeSeeder::class);
        $this->call(PartySeeder::class);
        $this->call(UserSeeder::class);
    }
}
