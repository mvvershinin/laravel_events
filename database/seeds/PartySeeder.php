<?php

use App\Models\GeoNode;
use App\Models\Party;
use Illuminate\Database\Seeder;

class PartySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cities = GeoNode::whereLevel(2)->get();

        foreach($cities as $city){
            $count = rand(1, 5);
            $party = [];
            for($i = 0; $i < $count; $i++){
                $party[] = [
                    'name' => 'random party '. base64_encode(random_bytes(10)),
                    'datetime' => now()->addDays($i)->addHours($i)->addMinutes($i),
                    'geo_node_id' =>$city->id
                ];
            }
            Party::insert($party);
        }

    }
}
