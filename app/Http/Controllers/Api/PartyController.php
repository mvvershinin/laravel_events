<?php

namespace App\Http\Controllers\Api;

use App\Contracts\PartyInterface;
use App\Http\Controllers\Controller;
use App\Models\Party;
use Illuminate\Http\Request;

class PartyController extends Controller
{
    protected $service;

    /**
     * MessageController constructor.
     * @param PartyInterface $service
     */
    public function __construct(PartyInterface $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of parties
     *
     * @return \Illuminate\Http\Response
     */
    public function index($offset = 0, $limit = 30)
    {
        return $this->service->index($offset, $limit);

    }


    /**
     * Display party with city and particiants.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->service->show($id);
    }
}
