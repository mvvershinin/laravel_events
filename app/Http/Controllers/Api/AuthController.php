<?php

namespace App\Http\Controllers\Api;

use App\Contracts\ApiAuth;
use App\Http\Requests\LoginRequest;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    protected $apiAuth;

    /**
     * MessageController constructor.
     * @param ApiAuth $apiAuth
     */
    public function __construct(ApiAuth $apiAuth)
    {
        $this->apiAuth = $apiAuth;
    }


    /**
     * Login user and create token
     *
     * @param LoginRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(LoginRequest $request)
    {
        return $this->apiAuth->login($request);
    }
}
