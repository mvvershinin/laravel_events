<?php

namespace App\Http\Controllers\Api;

use App\Contracts\MailInterface;
use App\Contracts\PartyParticipantInterface;
use App\Http\Controllers\Controller;

use App\Http\Requests\PartyPartisiantStoreRequest;
use App\Models\Party;


class PartyParticipantController extends Controller
{
    protected $service;
    protected $mailService;

    /**
     * MessageController constructor.
     * @param PartyParticipantInterface $service
     */
    public function __construct(PartyParticipantInterface $service, MailInterface $mail)
    {
        $this->service = $service;
        $this->mailService = $mail;
    }
    /**
     * create and add partisiants to party
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PartyPartisiantStoreRequest $request, $party)
    {
        $this->mailService->sendEmails($request);
        return $this->service->store($request, $party);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PartyPartisiantStoreRequest $request, $party)
    {
        $this->mailService->sendEmails($request);
        return $this->service->update($request, $party);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(PartyPartisiantStoreRequest $request, $party)
    {
        return $this->service->delete($request, $party);
    }
}
