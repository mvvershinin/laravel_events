<?php

namespace App\Http\Controllers\Api;

use App\Contracts\MailInterface;
use App\Contracts\ParticipantInterface;
use App\Http\Controllers\Controller;
use App\Http\Requests\PartisiantStoreRequest;
use App\Models\Participant;
use Illuminate\Http\Request;

class ParticipantController extends Controller
{
    protected $service;
    protected $mailService;

    /**
     * MessageController constructor.
     * @param ParticipantInterface $service
     */
    public function __construct(ParticipantInterface $service, MailInterface $mail)
    {
        $this->service = $service;
        $this->mailService = $mail;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($offset = 0, $limit = 30)
    {
        return $this->service->index($offset, $limit);
    }

    /**
     * create and add partisiants to party
     *
     * @param PartisiantStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(PartisiantStoreRequest $request)
    {
        $this->mailService->wellcome('gf');

        return $this->service->store($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->service->show($id);
    }
}
