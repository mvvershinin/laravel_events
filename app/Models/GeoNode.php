<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GeoNode extends Model
{
    protected $fillable = [
        'parent_id',
        'name',
        'level'
    ];

    public $timestamps = false;

    /**
     * relation with geo node on top level
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(GeoNode::class, 'parent_id');
    }
}
