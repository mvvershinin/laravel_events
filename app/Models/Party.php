<?php

namespace App\Models;

use App\City;
use Illuminate\Database\Eloquent\Model;

class Party extends Model
{
    protected $fillable = [
        'name',
        'datetime',
        'geo_node_id'
    ];

    /**
     * relation city for event
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo(GeoNode::class, 'geo_node_id');
    }

    /**
     * participants relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function participants()
    {
        return $this->belongsToMany(Participant::class, 'participant_party');
    }
}
