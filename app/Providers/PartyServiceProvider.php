<?php

namespace App\Providers;

use App\Services\PartyEloquent;
use Illuminate\Support\ServiceProvider;

class PartyServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind('App\Contracts\PartyInterface', function ($request) {
            return new PartyEloquent();
        });
    }
}
