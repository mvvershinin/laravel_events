<?php

namespace App\Providers;

use App\Services\ParticipantEloquent;
use Illuminate\Support\ServiceProvider;

class ParticipantServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind('App\Contracts\ParticipantInterface', function ($request) {
            return new ParticipantEloquent();
        });
    }
}
