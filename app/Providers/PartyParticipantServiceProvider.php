<?php

namespace App\Providers;

use App\Services\PartyParticipantEloquent;
use Illuminate\Support\ServiceProvider;

class PartyParticipantServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind('App\Contracts\PartyParticipantInterface', function () {
            return new PartyParticipantEloquent();
        });
    }
}
