<?php

namespace App\Providers;

use App\Contracts\MailInterface;
use App\Services\MailLog;
use App\Services\MailSmtp;
use Illuminate\Support\ServiceProvider;

class MailServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind('App\Contracts\MailInterface', function ($request) {
            return new MailLog();
        });
    }
}
