<?php


namespace App\Services;


use App\Contracts\MailInterface as MailInterfaceAlias;
use App\Contracts\ParticipantInterface;
use App\Models\Participant;

class ParticipantEloquent implements ParticipantInterface
{
    /**
     *
     * @param $offset
     * @param $limit
     * @return mixed
     */
    public function index($offset, $limit)
    {
        return Participant::offset($offset)->limit($limit)->get(['id', 'first_name', 'last_name', 'email' ]);
    }

    /**
     * show particiant
     *
     * @param $id
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     */
    public function show($id)
    {
        return Participant::with('parties.city.parent.parent')->find($id);
    }

    /**
     * create new particiant
     *
     * @param $request
     * @return Participant
     */
    public function store($request)
    {
        $partsiant = new Participant($request->toArray());
        $partsiant->save();

        return $partsiant;
    }
}
