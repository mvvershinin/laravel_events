<?php


namespace App\Services;


use App\Contracts\PartyParticipantInterface;
use App\Models\Party;

class PartyParticipantEloquent implements PartyParticipantInterface
{
    /**
     * create relations participans and party
     *
     * @param $request
     * @param $party
     * @return mixed
     */
    public function store($request, $party)
    {
        $item = Party::findOrFail($party);

        $item->participants()->sync($request->participants);

        return $item->load('participants');
    }

    /**
     * add to relation participans and party
     *
     * @param $request
     * @param $party
     * @return mixed
     */
    public function update($request, $party)
    {
        $item = Party::findOrFail($party);

        $item->participants()->attach($request->participants);

        return $item->load('participants');
    }

    /**
     * delete from relation participans and party
     *
     * @param $request
     * @param $party
     * @return mixed
     */
    public function delete($request, $party)
    {
        $item = Party::findOrFail($party);

        $item->participants()->detach($request->participants);

        return $item->load('participants');
    }
}
