<?php


namespace App\Services;


use App\Contracts\PartyInterface;
use App\Models\Party;

class PartyEloquent implements PartyInterface
{
    /**
     * list of parties
     *
     * @param $offset
     * @param $limit
     * @return mixed
     */
    public function index($offset, $limit)
    {
        return Party::offset($offset)->limit($limit)->get(['id', 'name', 'datetime', 'geo_node_id' ]);
    }

    /**
     * show party with participans and city
     *
     * @param $id
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     */
    public function show($id)
    {
        return Party::with('participants', 'city.parent.parent')->find($id);
    }
}
