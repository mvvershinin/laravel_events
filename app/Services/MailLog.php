<?php


namespace App\Services;


use App\Contracts\MailInterface;
use App\Models\Participant;
use Illuminate\Support\Facades\Log;

class MailLog implements MailInterface
{
    public function wellcome($email, $message = 'about wellcome to party service')
    {
        Log::info('send mail to ' . $email . ' ' . $message);
    }

    public function sendEmails($request)
    {
        $particiants = Participant::whereIn('id', $request->participants)->get('email')->pluck('email');
        foreach ($particiants as $email) {
            $this->wellcome($email, 'about add you to party');
        }
    }
}
