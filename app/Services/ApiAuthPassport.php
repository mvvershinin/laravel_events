<?php


namespace App\Services;

use App\Contracts\ApiAuth;
use App\Http\Requests\LoginRequest;
use Auth;

class ApiAuthPassport implements ApiAuth
{
    /**
     * login attempt and return user with bearer token
     *
     * @param LoginRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(LoginRequest $request)
    {
        $credentials = request(['name', 'password']);


        if (!(Auth::attempt($credentials))) {
            return response()->json([
                'status' => 'MESSAGE_AUTH_ERROR'
            ], 401);
        }

        $user = $request->user();
        if(!$user->email_verified_at){
            return response()->json([
                'status' => 'MESSAGE_AUTH_ERROR_EMAIL_NOT_CONFIRMED'
            ], 401);
        };

        $tokenResult = $user->createToken('personal');
        $token = $tokenResult->token;

        $token->save();

        return response()->json([
            'status' => 'success',
            'data' => Auth::user()
        ], 200)->header('Authorization', $tokenResult->accessToken);
    }
}
