<?php


namespace App\Contracts;


interface PartyInterface
{
    public function index($offset, $limit);
    public function show($id);
}
