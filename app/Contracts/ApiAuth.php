<?php


namespace App\Contracts;


use App\Http\Requests\LoginRequest;

interface ApiAuth
{
    public function login(LoginRequest $request);
}
