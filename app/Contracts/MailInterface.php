<?php


namespace App\Contracts;


interface MailInterface
{
    public function wellcome($email);
    public function sendEmails($sendEmails);
}
