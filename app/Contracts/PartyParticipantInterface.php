<?php


namespace App\Contracts;


interface PartyParticipantInterface
{
    public function store($request, $party);

    public function update($request, $party);

    public function delete($request, $party);
}
