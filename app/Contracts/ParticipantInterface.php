<?php


namespace App\Contracts;


interface ParticipantInterface
{
    public function index($offset, $limit);

    public function show($id);

    public function store($request);
}
