<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Активация нового пользователя</title>
</head>
<body>
<h1>Благодарим за регистрацию, {{$user->name}}!</h1>

<p>
    Перейдите <a href='{{ url("/user-verify/{$user->token}") }}'>по ссылке </a>чтобы завершить регистрацию
</p>
</body>
</html>
