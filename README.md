## как развернуть

- собираем laravel


    composer install
    
- конфиг


    .env.example -> .env

    php artisan key:generate

    php artisan migrate

    php artisan passport:install

    php artisan db:seed

    
## документация Api 
<https://documenter.getpostman.com/view/1341279/T17Di9Ye?version=latest#intro>


## Примечания
###докер 
использовал laradock(nginx + php-fpm + postgres)
 
Для работы с api необходим bearer token его можно получить после успешного логина в заголовке ответа
Сидеры создают тестовую структуру городов, мероприятия с привязкой к городам, пользователя
логин - admin@example.com
пароль - admin@example.com 
